import sys
import gpxpy
import gpxpy.gpx

if len(sys.argv) < 3:
    print("argv count=%d",len(sys.argv))
    #exit()
    
#要轉換的 GPX 檔名，建議直接放在 google 雲端硬碟比較方便
GPXFileName = sys.argv[1]         
#GPXFileName = "sample.gpx"

#轉換完成的 GPX 檔名。檔案產生後，可在左邊的檔案總館清單上看到，並下載
ExportFileName = sys.argv[2]     
#ExportFileName = "sample_invert.gpx"

# Parsing an existing file:
gpx_file = open(GPXFileName, 'r' , encoding="utf-8")

segmentList = []
wayPointList = []

gpx = gpxpy.parse(gpx_file)

for track in gpx.tracks:
    for segment in track.segments:
        pointList = []
        for point in segment.points:
            pointList.insert(0,point)
            #print('Point at ({0},{1}) -> {2}'.format(point.latitude, point.longitude, point.elevation))
        segmentList.insert(0,pointList)

for waypoint in gpx.waypoints:
    wayPointList.append(waypoint)
    #print('waypoint {0} -> ({1},{2})'.format(waypoint.name, waypoint.latitude, waypoint.longitude))

for route in gpx.routes:
    print('Route:')
    for point in route.points:
        print('Point at ({0},{1}) -> {2}'.format(point.latitude, point.longitude, point.elevation))

# There are many more utility methods and functions:
# You can manipulate/add/remove tracks, segments, points, waypoints and routes and
# get the GPX XML file from the resulting object:

#print('GPX:', gpx.to_xml())

gpxNew = gpxpy.gpx.GPX()

# Create first track in our GPX:
gpx_track = gpxpy.gpx.GPXTrack()
gpxNew.tracks.append(gpx_track)

# Create first segment in our GPX track:
for pointList in segmentList:
  #print("NewSegment")
  gpx_segment = gpxpy.gpx.GPXTrackSegment()
  gpx_track.segments.append(gpx_segment)
  # Create points:
  for point in pointList:
    gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(point.latitude, point.longitude, elevation=point.elevation))
    #print('NewPoint at ({0},{1}) -> {2}'.format(point.latitude, point.longitude, point.elevation))
    
for way in wayPointList:
  gpxNew.waypoints.append(way)


# You can add routes and waypoints, too...
#print('Created GPX:', gpx.to_xml())

text_file = open(ExportFileName, "w" , encoding="utf-8")
text_file.write(gpxNew.to_xml())
text_file.close()

print("轉換完成")